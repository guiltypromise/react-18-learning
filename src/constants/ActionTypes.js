export const ActionTypes = {

    // Cities actions
    REFRESH:  'REFRESH',
    READ: 'READ',
    ADD: 'ADD',

    // Error actions
    ADD_ERROR: 'ADD_ERROR',
    CLEAR_ERROR: 'CLEAR_ERROR',
    READ_ERROR: 'READ_ERROR',
}