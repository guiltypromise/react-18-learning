import React, { useState } from 'react';
import {
    Container,
    Row,
    Col,
  } from 'reactstrap';
import WeatherCard from '../weather-card/WeatherCard';
import { useDispatch, useSelector } from 'react-redux';


function Dashboard(props) {

    const {cities} = useSelector((state) => state.cities);

    return (
    <>
        {cities.map(city => (

            <Col xs="12" sm="6" className="bg-light border">
                <WeatherCard 
                    key={city.id} 
                    details={city} 
                />
            </Col>

        ))}
    </>
    )
};

export default Dashboard;