import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { useAddCityWeatherById } from '../../helpers/CityWeatherHelper';


// TODO: deeplinking where Weather Details page can fetch API on its own
function WeatherDetails() {

  const citiesState = useSelector((state) => state.cities);
  const dispatch = useDispatch();

  const currentUri = useLocation();
  const queryId = currentUri.pathname.split('/').pop();
  const selectedCityId = currentUri.state.cityId || queryId;

  let cityDetails = citiesState.cities.find((city) => city.id === selectedCityId);

  if(!cityDetails){
    dispatch(useAddCityWeatherById(selectedCityId));
    cityDetails = citiesState.cities.find((city) => city.id === selectedCityId);
  }



  return (
    <div className="text-center  px-2">
      <div className="mx-auto">
        <h2>Weather Details</h2>
        <div>{cityDetails.name}</div>
        <div>{cityDetails.main.temp}</div>
        <div>-Weather Icon</div>
      <div>-Forecast</div>
      </div>

    </div>
  )
}

export default WeatherDetails;