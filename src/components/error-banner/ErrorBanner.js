import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Alert from 'react-bootstrap/Alert';
import { ActionTypes } from '../../constants/ActionTypes';



export default function ErrorBanner(){

    const dispatch = useDispatch();

    const {isError, errorMsg} = useSelector((state) => state.errors.error);

    const handleAlertClose = () => {
        dispatch({type: ActionTypes.CLEAR_ERROR});
    }

    if(isError){
        return (
            <div className="pt-3">
                <Alert variant="danger" onClose={handleAlertClose} dismissible>
                    <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
                    <p>{errorMsg}</p>
                </Alert>
            </div>
          );
    } 
}