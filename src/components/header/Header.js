import React, { useState, useEffect } from 'react';

import { useAddCityWeather } from '../../helpers/CityWeatherHelper';
import { useDispatch, useSelector } from 'react-redux';



function Header(props) {

  const [city, setCityName] = useState('');

  const dispatch = useDispatch();

  const handleSubmit = async (event) => {
    event.preventDefault();
    dispatch(useAddCityWeather(city));
    setCityName('');
  };

  return (
    
    <div>
      <h1 href="/" className="fs-3"><em>{props.appName}</em></h1>
      <form onSubmit={handleSubmit}>
        <input 
          type="text"
          placeholder='Enter City Name' 
          value={city}
          onChange={event => setCityName(event.target.value)}
        />
        <button type='submit'>Submit</button>
      </form>

    </div>
  );
}

export default Header;