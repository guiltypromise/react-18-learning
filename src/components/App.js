import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Route,
  Routes
} from 'react-router-dom';
import {
  Col,
  Container,
  Row,
} from 'reactstrap';
import Dashboard from './dashboard/Dashboard';
import Header from './header/Header';
import ErrorBanner from './error-banner/ErrorBanner';
import WeatherDetails from './weather-details/WeatherDetails';


export function App({ initialData }) {

  return (
    <div>
        <Container fluid="lg">
          <ErrorBanner />
          <Row xs="12" sm="8" className="h-100 px-3" >
            <Col xs="12" sm="8" className="p-0 py-3">
              <Header appName={ initialData.appName } />
            </Col>
            <Routes>
              <Route exact 
                path="/" 
                element={
                  <Dashboard key={'DashboardCard'} />
                } 
              />
              <Route exact 
                path="/details/:cityId" 
                element={
                  <WeatherDetails />
                } 
              />
            </Routes>
          </Row>
        </Container>
    </div>
  );
}