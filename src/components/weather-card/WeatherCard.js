import React from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { ActionTypes } from '../../constants/ActionTypes';



const WeatherCard = (props) => {
    const navigateToPage = useNavigate();

    const dispatch = useDispatch();


    const handleNavigateToPage = () => {
        dispatch({type: ActionTypes.CLEAR_ERROR});
        navigateToPage(`/details/${props.details.id}`, 
        { state: 
          {
            cityId: props.details.id
          }
        }
      );
    };

  return (
    <div onClick={handleNavigateToPage} className=" px-2">
      <h2>Weather Summary</h2>
      <div>{props.details.name}</div>
      <div>{props.details.main.temp}</div>
      <div>-Low Temp</div>
      <div>-High Temp</div>
      <div>-Humidity</div>
    </div>
  );
}

export default WeatherCard;