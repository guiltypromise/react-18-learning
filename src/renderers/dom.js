import { App } from 'components/App';
import * as React from 'react';
import * as ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import WeatherStore from "../store/WeatherStore";

import '../styles/index.scss';

// Import all of Bootstrap's JS
import 'bootstrap';

const root = ReactDOM.hydrateRoot(document.getElementById('root'),
<React.StrictMode>
  <BrowserRouter>
    <Provider store={WeatherStore}>
      <App initialData={window.__R_DATA.initialData} />
    </Provider>
  </BrowserRouter>
</React.StrictMode>
);
