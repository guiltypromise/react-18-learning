import { App } from 'components/App';
import * as React from 'react';
import * as ReactDOMServer from 'react-dom/server';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom/server';
import WeatherStore from "../store/WeatherStore";

export async function serverRenderer() {
  const initialData = {
    appName: 'MyWeather',
  };

  const pageData = {
    title: `${initialData.appName}`,
  };

  if (typeof window === 'undefined') {
    global.window = {}
  }


  return Promise.resolve({
    initialData,
    initialMarkup: ReactDOMServer.renderToString(
      <StaticRouter>
       <Provider store={WeatherStore}>
          <App initialData={initialData} />
        </Provider>
      </StaticRouter>
    ),
    pageData,
  });
}
