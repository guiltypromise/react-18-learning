
import {ActionTypes} from '../constants/ActionTypes';


const citiesReducer = (state = {cities: []}, action) => {
    switch( action.type ) {
        case ActionTypes.ADD:
            return { ...state, cities: [ ...state.cities, action.city]};
        case ActionTypes.READ:
            return state.cities;
        default:
            return state;
    }
};

export default citiesReducer;