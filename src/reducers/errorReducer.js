
import {ActionTypes} from '../constants/ActionTypes';


const errorReducer = (state = {
        error: {
            isError: false,
            errorMsg: '',
        }
    }, payload) => {

    switch( payload.type ) {
        case ActionTypes.ADD_ERROR:
            return { ...state, error: {isError: payload.action.isError, errorMsg: payload.action.errorMsg}};
        case ActionTypes.READ_ERROR:
            return state.error;
        case ActionTypes.CLEAR_ERROR:
            return { ...state, error: {isError: false, errorMsg: ''}}
        default:
            return state;
    }
};

export default errorReducer;