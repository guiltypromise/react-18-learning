import {ActionTypes} from '../constants/ActionTypes';
import axios from 'axios';

// TODO: localStore of selected cities and refresh of selected cities
// hint: https://api.openweathermap.org/data/2.5/group?id=4671654,4726206&appid=1fdd04ea27c806e5b098d0e15542293e&units=imperial

async function _getCityWeather(cityName) {
    return await fetchWeatherApi(cityName, 'cityName');;
}

async function _getCityWeatherByCityId(cityId){
    return await fetchWeatherApi(cityId, 'cityId');
}

async function fetchWeatherApi(identifier, idType){
    const API_KEY = "1fdd04ea27c806e5b098d0e15542293e";
    const TEMP_UNIT_OF_MEASURE = 'imperial';

    const baseUri = 'https://api.openweathermap.org/data/2.5/weather?';
    const apiKey = `&appid=${API_KEY}`;
    const units = `&units=${TEMP_UNIT_OF_MEASURE}`;
    let query;

    switch(idType){
        case 'cityName':
            query = `q=${identifier}`;
            break;
        case 'cityId':
            query = `id=${identifier}`;
            break;
        default:
            break;
    }

    try {
        const response = await axios.get(`${baseUri}${query}${apiKey}${units}`);
        return response.data;
    } catch (e) {
        throw new Error(JSON.stringify(e.response.data));
    }
}


export function useAddCityWeather(cityName) {
    return async function addCityWeatherThunk( dispatch, getState) {
        // PLuralisght Calling APIs with React 17, Using Redux Thunk Part 1
        // stores POST to DB through await fetch. 
        // We're not using a DB

        try {
            const city = await _getCityWeather(cityName);
            dispatch({type: ActionTypes.ADD, city});
        } catch(error) {
            const res = JSON.parse(error.message);
            dispatch({type: ActionTypes.ADD_ERROR, action: {isError: true, errorMsg: `${res.message}` } });
        }
    }
}

export function useAddCityWeatherById(cityId) {
    return async function addCityWeatherThunk( dispatch, getState) {
        try {
            const city = await _getCityWeatherByCityId(cityId);
            dispatch({type: ActionTypes.ADD, city});
        } catch(error) {
            const res = JSON.parse(error.message);
            dispatch({type: ActionTypes.ADD_ERROR, action: {isError: true, errorMsg: `${res.message}` }});
        }
    }
}