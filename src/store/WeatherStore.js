import { configureStore } from '@reduxjs/toolkit'
import citiesReducer from '../reducers/CitiesReducer';
import errorReducer from '../reducers/ErrorReducer';


const WeatherStore = configureStore({
  reducer: {
    cities: citiesReducer,
    errors: errorReducer
  }
}); // The thunk middleware was automatically added


export default WeatherStore;